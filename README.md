Hi, This playbook may be needs additional module.

- ansible-galaxy collection install community.general
- ansible-galaxy collection install ansible.posix

##NOTES
********************************************************************************************
## Example of sysctl Kernel Parameter File

#Created by Thava Abyres.SDN.BHD
#
#
fs.file-max=6815744
fs.aio-max-nr=1048576
kernel.shmmax=386547056640
kernel.sem=2200 32000 200 128
net.core.rmem_default=16777216
net.core.rmem_max=16777216
net.core.wmem_default=16777216
net.core.wmem_max=16777216
net.core.netdev_max_backlog=500000
net.ipv4.tcp_tw_reuse=1
net.ipv4.tcp_timestamps=0
net.ipv4.tcp_sack=0
net.ipv4.tcp_rmem=4096 87380 16777216
net.ipv4.tcp_wmem=4096 65536 16777216
net.ipv4.ip_local_port_range=1024 65535
vm.swappiness=0
vm.dirty_background_ratio=3
vm.dirty_ratio=15
vm.dirty_expire_centisecs=500
vm.dirty_writeback_centisecs=100
vm.vfs_cache_pressure=100
######Hugepage#######
vm.nr_hugepages=16384
#####################
vm.hugetlb_shm_group=54322
*******************************************************************************************
## Example of security limit config file

##Created by Thava Abyres.SDN.BHD
#
#
#
#
oracle	soft	memlock	unlimited
oracle	hard	memlock	unlimited
oracle	soft	nproc	131072
oracle	hard	nproc	131072
oracle	soft	nofile	131072
oracle	hard	nofile	131072
oracle	soft	stack	10240
oracle	hard	stack	32768

grid	soft	memlock	unlimited
grid	hard	memlock	unlimited
grid	soft	nproc	131072
grid	hard	nproc	131072
grid	soft	nofile	131072
grid	hard	nofile	131072
grid	soft	stack	10240
grid	hard	stack	32768

********************************************************************************************
## How to calculate Hugepage value?
(Reference: https://www.carajandb.com/en/blog/2016/7-easy-steps-to-configure-hugepages-for-your-oracle-database-server/ )

SGA / Hugepagesize = Number Hugepages

Hugepagesize: grep Hugepagesize /proc/meminfo

Example:
# GB must converted to KB
32GB / 2MB = 16384 KB
33554432 / 2048 = 16384

Happy Automating!!
by, Thava

